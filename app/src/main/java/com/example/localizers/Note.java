package com.example.localizers;

public class Note {
    private String titre;
    private String contenu;
    //Ajouter un champ pour les coordonnées GPS

    public Note(String titre, String contenu)
    {
        this.titre =titre;
        this.contenu = contenu;
    }

    public String getTitre(){
        return titre;
    }

    public String getContenu(){
        return contenu;
    }

    //getCoordonnée
}
