package com.example.localizers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ListeNotes {

    private ArrayList<Note> listeNote;
    private Context appContext;
    private NotesDBOpenHelper dbHelper;

    public ListeNotes(Context ctx)
    {
        listeNote = new ArrayList<>();
        AjoutExempleNote();

        appContext = ctx.getApplicationContext();
        dbHelper = new NotesDBOpenHelper(appContext, "DBnotes", null, 1);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //Rajouter la colonne des coordonnées GPS

        Cursor cursor = db.query(NotesDBOpenHelper.TABLE, new String[]{NotesDBOpenHelper.COLUMN_TITRE, NotesDBOpenHelper.COLUMN_CONTENU}, null, null, null, null, null);
        try {
            while (cursor.moveToNext()) {
                listeNote.add(new Note(cursor.getString(0),cursor.getString(1)));
            }
        }
        finally {
            cursor.close();
        }
        db.close();
    }

    public int getNbNote()
    {
        return listeNote.size();
    }

    //Manque les coordonnées GPS
    public void creerNote(String titre, String contenu)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues toAdd = new ContentValues();

        toAdd.put(NotesDBOpenHelper.COLUMN_TITRE, titre);
        toAdd.put(NotesDBOpenHelper.COLUMN_CONTENU, contenu);

        db.insert(NotesDBOpenHelper.TABLE, null, toAdd);
        db.close();

        listeNote.add(new Note(titre, contenu));
    }

    public boolean effacerNote(String titre)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(NotesDBOpenHelper.TABLE, NotesDBOpenHelper.COLUMN_TITRE + " = ?", new String[]{titre});
        db.close();

        for(int i = 0; i < listeNote.size(); i++)
        {
            Note n = listeNote.get(i);

            if(n.getTitre() == titre)
            {
                listeNote.remove(i);
                return true;
            }
        }

        return false;
    }

    private void AjoutExempleNote()
    {
        creerNote("Titre 1", "Contenu de la note 1");
        creerNote("Titre 2", "Deuxième contenu avec des accents");
        creerNote("Titre 3", "Paturiku Barukani est un lycéen japonais ayant quelques difficultés\n" +
                "sociales et vivant la plupart du temps reclu chez lui, à jouer aux jeuxvidéo lorsqu'il ne regarde pas des animés Isekai lorsqu'il ne lit pas des\n" +
                "Isekai lorsqu'il ne dort pas. Ainsi était la vie de Patriku Balukani. ");
    }
}
