package com.example.localizers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class NotesDBOpenHelper extends SQLiteOpenHelper {

    public static final String TABLE ="notes";
    public static final String COLUMN_ID="id";
    public static final String COLUMN_TITRE="titre";
    public static final String COLUMN_CONTENU="contenu";
    //champ pour les coordonnées GPS

    public NotesDBOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String requete = "create table "+ TABLE +" ( " +
                COLUMN_ID + " integer primary key , " +
                COLUMN_TITRE + " text not null ," +
                COLUMN_CONTENU + " text not null " +
                //Rajouter la colonne des coordonnées GPS
                ") ;";

        db.execSQL(requete);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //restera vide en théorie
    }
}
